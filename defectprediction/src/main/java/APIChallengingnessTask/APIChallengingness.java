package APIChallengingnessTask;

import buggycodeannotator.CommitFinder;
import org.apache.lucene.queryparser.classic.ParseException;
import org.eclipse.jgit.api.errors.GitAPIException;
import utility.StaticSharedData;
import utility.UtilFunctions;

import java.io.*;
import java.util.*;

public class APIChallengingness {
    // guru_csv --> Project path
    private Map<String, String> guruPath;

    // commit --> buggy for all commits
    private Map<String, Boolean> commitsBuggy;

    // commit --> List of APIs in it for all projects
    private Map<String, List<String>> commitAPIs;

    // commit --> List of deleted APIs in it for all projects
    private Map<String, List<String>> deletedCommitAPIs;

    // api --> challenging, just to store api_scores.csv scores in RAM
    private Map<String, Double> apiChallenging;

    // api --> challengingness, how many buggy commit, how many non-buggy commit
    private Map<String, List<Double>> finalResult;

    public APIChallengingness() {
        commitsBuggy = new HashMap<>();
        apiChallenging = new HashMap<>();
        commitAPIs = new HashMap<>();
        finalResult = new HashMap<>();
        guruPath = new HashMap<>();

        // This HashMap is useless :-?
        guruPath.put("1", "/home/tahmooresi/projects/FastHub/.git");
        guruPath.put("2", "/home/tahmooresi/projects/spring-petclinic/.git");

        try {
            traverseGuruResultFolder(StaticSharedData.COMMIT_GURU_DIR);
            extractAPIChallenging(StaticSharedData.API_SCORES);
            extractCommitAPIs();
            accumulateFinalResults();
            saveResultToFile();
        } catch (IOException | ParseException | GitAPIException e) {
            e.printStackTrace();
        }
    }

    private void saveResultToFile() throws IOException {
//        int a = 0;
//        for (String api : finalResult.keySet()) {
//            System.out.println(api + ": " +
//                    Math.round(finalResult.get(api).get(0)) + " " +
//                    finalResult.get(api).get(1) + " " +
//                    finalResult.get(api).get(2));
//            if (a++ == 10)
//                break;
//        }

        FileOutputStream fileStream = new FileOutputStream("apichallenginness.csv");
        PrintStream printStream = new PrintStream(fileStream);
        printStream.println("api,challengingness,inbuggy,innonbuggy");
        for (String api : finalResult.keySet()) {
            printStream.println(api + "," + finalResult.get(api).get(0) + "," + finalResult.get(api).get(1) +
                    finalResult.get(api).get(2));
        }
        printStream.close();
        fileStream.close();

    }

    private void accumulateFinalResults() {
        for (String api : apiChallenging.keySet()) {
            List<Double> res = Arrays.asList(apiChallenging.get(api), 0d, 0d);
            finalResult.put(api, res);
        }

        for (String commit : commitsBuggy.keySet()) {
            if (!commitAPIs.containsKey(commit))
                continue;

            for (String api : commitAPIs.get(commit)) {
                if (!finalResult.containsKey(api))
                    continue;

                if (commitsBuggy.get(commit))
                    finalResult.get(api).set(1, finalResult.get(api).get(1) + 1);
                else finalResult.get(api).set(2, finalResult.get(api).get(2) + 1);
            }
        }
    }

    private void extractCommitAPIs() throws ParseException, GitAPIException, IOException {
        for (String guru_csv : guruPath.keySet()) {
            String projectPath = guruPath.get(guru_csv);
            analyzeRepo(projectPath);
        }
    }

    private void traverseGuruResultFolder(String folder) throws IOException {
        for (String fileName : UtilFunctions.listFilesForFolder(folder)) {
            Map<String, Boolean> commitBuggy = new HashMap<>();

            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line;
            br.readLine(); // skip header
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] cols = line.split(",");
                commitBuggy.put(cols[0], Boolean.valueOf(cols[9]));
            }

            commitsBuggy.putAll(commitBuggy);
            br.close();
        }
    }

    private void extractAPIChallenging(String pathToApiScores) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(pathToApiScores));
        String line;
//        br.readLine(); // skip header
        while ((line = br.readLine()) != null) {
            // use comma as separator
            String[] cols = line.split(",");
            apiChallenging.put(cols[0], Double.valueOf(cols[1]));
        }
    }

    private void analyzeRepo(String path) throws IOException, GitAPIException, ParseException {
        // TODO: Not all of the process in annotateCommits method is necessary!
        /* Initialize Repository */
        CommitFinder commitFinder = new CommitFinder(path);
        commitFinder.initializeRepository();

        /* Run Program */
        commitFinder.extractCommitAPIs();

        // commit --> List of APIs
        Map<String, List<String>> commitAPIs = commitFinder.getCommitAPIs();
        Map<String, List<String>> deletedCommitAPIs = commitFinder.getDeletedCommitAPIs();
        this.commitAPIs.putAll(commitAPIs);
        this.deletedCommitAPIs.putAll(deletedCommitAPIs);
    }

    public static void main(String[] args) {
        new APIChallengingness();
    }
}
