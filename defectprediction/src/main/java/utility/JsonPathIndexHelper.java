package utility;

import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Document;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class JsonPathIndexHelper {
    /**
     * Annotations $.question.informationUnits[?(@.type=='CodeTaggedUnit')]
     * .astNode..[?(@.type=='AnnotationNode')].identifier..name
     **/
    public static void updateAPIType(List<String> items, HashMap<String, String> apiType, String type) {
        for (String item : items) {
            apiType.put(item, type);
        }
    }

    public static void indexAnnotations(JSONObject json,
                                        HashMap<String, Integer> numInCode,
                                        HashMap<String, String> apiType) {
        List<String> annotations = JsonPath.read(json.toString(),
                "$.question.informationUnits[?(@.type=='CodeTaggedUnit')]"
                        + ".astNode..[?(@.type=='AnnotationNode')].identifier..name");

        addToNum(numInCode, annotations);
        updateAPIType(annotations, apiType, "Annotation");
//        addToNum(numInAllCodes, annotations);
    }

    /**
     * Class Declaration $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='ClassDeclarationNode')].identifier.name
     **/
    public static void indexClassDeclaration(JSONObject json,
                                             HashMap<String, Integer> numInCode,
                                             HashMap<String, String> apiType) {
        List<String> classDeclarations = JsonPath.read(json.toString(),
                "$.question.informationUnits.[?(@.type=='CodeTaggedUnit')]"
                        + ".astNode..[?(@.type=='ClassDeclarationNode')].identifier.name");
        updateAPIType(classDeclarations, apiType, "ClassDeclaration");
        addToNum(numInCode, classDeclarations);
//        addToNum(numInAllCodes, classDeclarations);
    }

    /**
     * Method Invocation // just name of method - System.out.println() -> just
     * println $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='MethodInvocationNode')].identifier.name
     **/
    public static void indexMethodInvocation(JSONObject json,
                                             HashMap<String, Integer> numInCode,
                                             HashMap<String, String> apiType) {
        List<String> methodInvocations = JsonPath.read(json.toString(),
                "$.question.informationUnits.[?(@.type=='CodeTaggedUnit')]"
                        + ".astNode..[?(@.type=='MethodInvocationNode')].identifier.name");
        updateAPIType(methodInvocations, apiType, "MethodInvocation");
        addToNum(numInCode, methodInvocations);
//        addToNum(numInAllCodes, methodInvocations);

    }

    public static void indexCodeElement(String jsonPathQuery, String codeElementType, JSONObject json,
                                        HashMap<String, Integer> numInCode,
                                        HashMap<String, String> apiType) {
        List<String> elements = JsonPath.read(json.toString(), jsonPathQuery);
        updateAPIType(elements, apiType, codeElementType);
        addToNum(numInCode, elements);
//        addToNum(numInAllCodes, imports);
    }

    /**
     * Imports $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='ImportDeclarationNode')].identifier..name
     **/
    public static void indexImport(JSONObject json,
                                   HashMap<String, Integer> numInCode,
                                   HashMap<String, String> apiType) {
        List<String> imports = JsonPath.read(json.toString(),
                "$.question.informationUnits.[?(@.type=='CodeTaggedUnit')]."
                        + "astNode..[?(@.type=='ImportDeclarationNode')].identifier..name");
        updateAPIType(imports, apiType, "Import");
        addToNum(numInCode, imports);
//        addToNum(numInAllCodes, imports);
    }

    /**
     * Class Usage ( parameterizedtypenode ?)
     * $.question.informationUnits[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='ParameterizedTypeNode')].identifier.name
     * $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='CatchTypeNode')]..name
     **/
    public static void indexClassUsage(JSONObject json,
                                       HashMap<String, Integer> numInCode,
                                       HashMap<String, String> apiType) {
        List<String> usages = JsonPath.read(json.toString(), "$.question.informationUnits[?(@.type=='CodeTaggedUnit')]."
                + "astNode..[?(@.type=='ParameterizedTypeNode')].identifier.name");
        List<String> catches = JsonPath.read(json.toString(),
                "$.question.informationUnits.[?(@.type=='CodeTaggedUnit')]"
                        + ".astNode..[?(@.type=='CatchTypeNode')]..name");
        updateAPIType(usages, apiType, "ClassUsage");
        updateAPIType(catches, apiType, "ClassUsage");
        addToNum(numInCode, usages);
        addToNum(numInCode, catches);
//        addToNum(numInAllCodes, usages);
//        addToNum(numInAllCodes, catches);
    }

    /**
     * Method Declaration $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='MethodDeclarationNode')].identifier.name
     **/
    public static void indexMethodDeclarations(JSONObject json,
                                               HashMap<String, Integer> numInCode,
                                               HashMap<String, String> apiType) {
        List<String> methods = JsonPath.read(json.toString(),
                "$.question.informationUnits.[?(@.type=='CodeTaggedUnit')]"
                        + ".astNode..[?(@.type=='MethodDeclarationNode')].identifier.name");
        updateAPIType(methods, apiType, "MethodDeclarations");
        addToNum(numInCode, methods);
//        addToNum(numInAllCodes, methods);
    }

    public static void indexTitle(JSONObject json,
                                  HashMap<String, Integer> numInCode,
                                  HashMap<String, Integer> numInTitle) {
        String title = JsonPath.read(json.toString(), "$.question.title");
        HashMap<String, Integer> results = searchListInText(title, numInCode.keySet());

        for (String key : results.keySet()) {
            addToNum(numInTitle, key, results.get(key));
//            addToNum(numInAllTitle, key, results.get(key));
        }

    }

    public static void indexText(String prefix, JSONObject json,
                                 HashMap<String, Integer> numInCode,
                                 HashMap<String, Integer> numInText) {
        List<String> texts = JsonPath.read(json.toString(),
                prefix + "[?(@.type=='NaturalLanguageTaggedUnit')].rawText");

        for (String text : texts) {
            HashMap<String, Integer> results = searchListInText(text, numInCode.keySet());

            for (String key : results.keySet()) {
                addToNum(numInText, key, results.get(key));
//                addToNum(numInAllText, key, results.get(key));
            }
        }
    }

    public static void addToNum(HashMap<String, Integer> num, Iterable<String> keys) {
        for (String key : keys) {
            if (num.keySet().contains(key)) {
                num.put(key, num.get(key) + 1);
            } else {
                num.put(key, 1);
            }
        }
    }

    public static void addToNum(HashMap<String, Integer> num, String key, int tedad) {
        if (num.keySet().contains(key)) {
            num.put(key, num.get(key) + tedad);
        } else {
            num.put(key, 1);
        }
    }

    public static HashMap<String, Integer> searchListInText(String text, Set<String> list) {
        HashMap<String, Integer> result = new HashMap<>();
        for (String term : list) {
            int matches = StringUtils.countMatches(text.toLowerCase(), term.toLowerCase());
            if (matches > 0) {
                result.put(term, matches);
            }
        }
        return result;
    }
}
