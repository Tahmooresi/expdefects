package utility;

import buggycodeannotator.CommitFinder;
import org.apache.lucene.queryparser.classic.ParseException;
import org.eclipse.jgit.api.errors.EmtpyCommitException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static java.lang.Math.max;


public class UtilFunctions {
//    private static UtilFunctions instance;
//    private UtilFunctions(){}
//    public static UtilFunctions getInstance() {
//        if (instance == null)
//            instance = new UtilFunctions();
//        return instance;
//    }

    public static Map<String, List<String>> getFilesAndAPIsInCommit(String commitHash, String projectPath) throws IOException, GitAPIException, ParseException {
//        projectPath = "/home/szamani/stackoverflow-challenging/.git"

        /* Initialize Repository */
        CommitFinder commitFinder = new CommitFinder(projectPath);
        commitFinder.initializeRepository();

        for (RevCommit commit : commitFinder.findAllCommits()) {
            if (commit.name().equals(commitHash)) {
                return commitFinder.extractCommitPresentAPIsByFile(commit);
            }
        }
        return null;
    }

    public static List<String> getFileChangesAfterSpecificCommit(String commitHash, String fileName, String projectPath) throws IOException, GitAPIException, ParseException {
        // commitHash = "1c830f8c55c5f0aa002f3f8dfcce55e1d1ece9db"
        // fileName = "src/buggycodeannotator/CommitFinder.java"
        // projectPath = "/home/projects/whatever/.git"

        RevCommit correspondingCommit = null;
        CommitFinder commitFinder = new CommitFinder(projectPath);
        commitFinder.initializeRepository();
        List<String> resultCommits = new ArrayList<>();

        for (RevCommit commit : commitFinder.findAllCommits())
            if (commit.name().equals(commitHash)) {
                correspondingCommit = commit;
                break;
            }

        if (correspondingCommit == null)
            throw new EmtpyCommitException("Commit not found");

        for (RevCommit commit : commitFinder.findAllCommits()) {
            if (commit.getCommitTime() > correspondingCommit.getCommitTime()) {
                List<String> changedFiles = commitFinder.getChangedFiles(commit);
                if (changedFiles.contains(fileName))
                    resultCommits.add(commit.name());
            }
        }

//        for (String fp : resultCommits)
//            System.out.println(fp);

        return resultCommits;
    }

    public static List<String> getAPIsByCommitHash(String commitHash, String projectPath) throws IOException, GitAPIException, ParseException {
//        commitHash = "702966a37e1ac7624d23a89647905b1a10360799";
//        projectPath = "/home/szamani/Desktop/projects/FastHub/.git";

        CommitFinder commitFinder = new CommitFinder(projectPath);
        commitFinder.initializeRepository();

        /* Run Program */
        commitFinder.extractCommitAPIs();

        // commit --> List of APIs
        Map<String, List<String>> commitAPIs = commitFinder.getCommitAPIs();

        for (RevCommit commit : commitFinder.findAllCommits()) {
            if (commit.name().equals(commitHash)) {
                return commitAPIs.get(commit);
            }
        }
        return null;
    }


    public static HashMap<String, Double> getPostScoresFromFile() throws FileNotFoundException {
        HashMap<String, Double> res = new HashMap<>();
        Scanner in = new Scanner(new File(StaticSharedData.POST_SCORES));
        while (in.hasNextLine()) {
            String line = in.nextLine();
            String[] d = line.split(",");
            double netScore = max(0, Double.valueOf(d[5]) - Double.valueOf(d[6]));
            res.put(d[0], netScore);
        }
        in.close();
        return res;
    }

    public static HashMap<String, Double> getAPIScoresFromFile() throws FileNotFoundException {
        HashMap<String, Double> res = new HashMap<>();
        Scanner in = new Scanner(new File(StaticSharedData.API_SCORES));
        while (in.hasNextLine()) {
            String line = in.nextLine();
            String[] d = line.split(",");
            res.put(d[0], Double.valueOf(d[3]));
        }
        in.close();
        return res;
    }

    public static List<String> listFilesForFolder(final String path) {
        final File folder = new File(path);
        List<String> filesInFolder = new ArrayList<>();
        // we know for sure that no directory is in this folder
        for (final File fileEntry : folder.listFiles())
            filesInFolder.add(fileEntry.getAbsolutePath());

        return filesInFolder;
    }
}
