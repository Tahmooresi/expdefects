package main.utils;

import java.sql.Timestamp;

/**
 * Created by HTahmooresi on 9/29/2018.
 */
public class Loggable {
    public void log(String message) {
        System.out.println(new Timestamp(new java.util.Date().getTime()) + " - " + message);
    }
}
