package main.utils;

import org.apache.commons.csv.CSVRecord;

import java.util.*;

/**
 * Created by HTahmooresi on 10/10/2018.
 */
public class FileData {

    private String name;
    private Integer loc;
    private List<String> involvedInBuggyCommits = new ArrayList<>();
    private List<String> apis = new ArrayList<>();
    private Map<Feature, Double> features = new HashMap<>();
    private List<String> apisWithScore = new ArrayList<>();
    public void setFeature(Feature feature, Double challenge) {
        this.features.put(feature, challenge);
    }

    public double getFeature(Feature feature) {
        return this.features.getOrDefault(feature, null);
    }
    public List<Feature> getFeatures(){
        Set<Feature> set = this.features.keySet();
        List<Feature> res = new ArrayList<>(set);
        Collections.sort(res);
        return res;
    }
    public List<String> getData(){
        List<String> res = new ArrayList<>();
        res.add(name);
        res.add(String.valueOf(loc));
        res.add(String.valueOf(getPostReleaseDefectsCount()));
        this.apisWithScore.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Double score1 = Double.valueOf(o1.split(":")[1]);
                Double score2 = Double.valueOf(o2.split(":")[1]);
                if (score1 > score2)
                    return -1;
                else if (score1 < score2)
                    return 1;
                return 0;
            }
        });
        res.add(String.join(" ",this.apisWithScore));
        return res;
    }
    public FileData() {
        this.apis = new ArrayList<>();
    }
    public static List<String> getHeader() {
        ArrayList<String> res = new ArrayList<>();
        res.add("File");
        res.add("loc");
        res.add("PostReleaseDefectsCount");
        res.add("APIs");
        return res;
    }
    public Integer getPostReleaseDefectsCount(){
        return involvedInBuggyCommits.size();
    }
    public String getName() {
        return name;
    }

    public Integer getLoc() {
        return loc;
    }

    public List<String> getApis() {
        return apis;
    }

    public void SetData(CSVRecord csvRecord) {
        this.name = csvRecord.get(0);
        this.loc = Integer.valueOf(csvRecord.get(1));
        this.involvedInBuggyCommits = Arrays.asList(csvRecord.get(2).split("\\s+"));
        this.apis = Arrays.asList(csvRecord.get(4).split("\\s+"));
    }

    public List<String> getApisWithScore() {
        return apisWithScore;
    }

    public void setApisWithScore(List<String> apisWithScore) {
        this.apisWithScore = apisWithScore;
    }
}
