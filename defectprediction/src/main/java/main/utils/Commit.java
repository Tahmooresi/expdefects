package main.utils;

import org.apache.commons.csv.CSVRecord;

import java.util.*;

/**
 * Created by HTahmooresi on 9/29/2018.
 */
public class Commit {
    private String hash;

    private boolean isBuggy;
    private int date;
    private Map<Feature, Double> features = new HashMap<>();
    private List<String> apis;
    private String ns;
    private String nd;
    private String nf;
    private String entrophy;
    private String la;
    private String ld;
    private String lt;
    private String ndev;
    private String age;
    private String nuc;
    private String exp;
    private String rexp;
    private String sexp;

    public Commit() {
        this.apis = new ArrayList<>();
    }

    public static List<String> getHeader() {
        ArrayList<String> res = new ArrayList<>();
        res.add("CommitHash");
        res.add("isBuggy");
        res.add("ns");
        res.add("nd");
        res.add("nf");
        res.add("entrophy");
        res.add("la");
        res.add("ld");
        res.add("lt");
        res.add("ndev");
        res.add("age");
        res.add("nuc");
        res.add("exp");
        res.add("rexp");
        res.add("sexp");
        return res;
    }

    public void setData(CSVRecord csvRecord) {
        hash = csvRecord.get(0);
        isBuggy = csvRecord.get(9).toLowerCase().equals("true");
        ns = csvRecord.get(11);
        nd = csvRecord.get(12);
        nf = csvRecord.get(13);
        entrophy = csvRecord.get(14);
        la = csvRecord.get(15);
        ld = csvRecord.get(16);
        lt = csvRecord.get(18);
        ndev = csvRecord.get(19);
        age = csvRecord.get(20);
        nuc = csvRecord.get(21);
        exp = csvRecord.get(22);
        rexp = csvRecord.get(23);
        sexp = csvRecord.get(24);
        date = Integer.valueOf(csvRecord.get(2));
    }

    public List<String> getData() {
        List<String> res = new ArrayList<>();
        res.add(hash);
        res.add(isBuggy() ? "1" : "0");
        res.add(ns);
        res.add(nd);
        res.add(nf);
        res.add(entrophy);
        res.add(la);
        res.add(ld);
        res.add(lt);
        res.add(ndev);
        res.add(age);
        res.add(nuc);
        res.add(exp);
        res.add(rexp);
        res.add(sexp);
        return res;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public boolean isBuggy() {
        return isBuggy;
    }

    public void setBuggy(boolean buggy) {
        isBuggy = buggy;
    }

    public void setFeature(Feature feature, Double challenge) {
        this.features.put(feature, challenge);
    }

    public double getFeature(Feature feature) {
        return this.features.getOrDefault(feature, null);
    }

    public List<Feature> getFeatures() {
        Set<Feature> set = this.features.keySet();
        List<Feature> res = new ArrayList<>(set);
        Collections.sort(res);
        return res;
    }

    public List<String> getApis() {
        return apis;
    }

    public void setApis(List<String> apis) {
        this.apis = apis;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }
}
