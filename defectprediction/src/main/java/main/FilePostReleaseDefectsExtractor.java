package main;

import buggycodeannotator.CommitFinder;
import main.utils.CSVWriter;
import main.utils.Commit;
import main.utils.Loggable;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.treewalk.TreeWalk;
import utility.UtilFunctions;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by HTahmooresi on 10/9/2018.
 */
public class FilePostReleaseDefectsExtractor extends Loggable {

    private Map<String, Commit> commits = new HashMap<>();
    private ArrayList<RevCommit> revCommits;
    private CommitFinder commitFinder;
    private Map<String, List<Commit>> postReleaseDefects = new HashMap<>();
    private Map<String, Integer> fileLOC = new HashMap<>();
    private Map<String, List<String>> filesAPIs = new HashMap<>();

    public void loadCommitsData(String path) throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(path);
        for (CSVRecord csvRecord : csvParser) {
            Commit commit = new Commit();
            commit.setData(csvRecord);
            commits.put(commit.getHash(), commit);
        }
    }

    public Map<String, List<Commit>> extractFilesCommitsMap(String projectPath) throws IOException, GitAPIException {
        commitFinder = new CommitFinder(projectPath);
        commitFinder.initializeRepository();
        revCommits = commitFinder.findAllCommits();
        HashMap<String, List<Commit>> filesCommits = new HashMap<>();
        for (RevCommit commit : revCommits) {
            List<String> files = commitFinder.getChangedFiles(commit);
            if (commits.get(commit.getName()) == null) {
                continue;
            }
            for (String file : files) {
                if (!file.endsWith(".java")) {
                    continue;
                }
                if (!filesCommits.containsKey(file)) {
                    filesCommits.put(file, new ArrayList<>());
                }
                List<Commit> commitsOfTheFile = filesCommits.get(file);
                commitsOfTheFile.add(commits.get(commit.getName()));
            }
        }
        return filesCommits;
    }

    public List<String> getFilesAndAPIsInCommit(String commitHash) throws IOException {
        for (RevCommit commit : revCommits) {
            if (!commit.getName().equals(commitHash))
                continue;

            List<String> javaFilesInCommit = new ArrayList<>();
            TreeWalk treeWalk = new TreeWalk(commitFinder.getRepository());
            treeWalk.addTree(commit.getTree());
            treeWalk.setRecursive(true);
            while (treeWalk.next()) {
                String path = treeWalk.getPathString();
                if (path.endsWith(".java")) {
                    javaFilesInCommit.add(path);
                    int loc = new String(commitFinder.getRepository().open(TreeWalk.forPath(commitFinder.getRepository(), path, commit.getTree()).getObjectId(0)).getBytes(), "utf-8").split("\r\n|\r|\n").length;
                    fileLOC.put(path, loc);
                }
            }
            this.filesAPIs = commitFinder.extractCommitPresentAPIsByFile(commit);
            return javaFilesInCommit;
        }
        return null;
    }

    public void extractPostReleaseDefects(String commitHash, Integer interval, Map<String, List<Commit>> filesCommits) throws IOException, GitAPIException {
        List<String> filesInCommit = getFilesAndAPIsInCommit(commitHash);
        for (String file : filesInCommit) {
            List<Commit> buggyCommits = new ArrayList<>();
            if (!filesCommits.containsKey(file)) {
                continue;
            }
            for (Commit commit : filesCommits.get(file)) {
                if (commit == null  || commits.get(commitHash) == null || !(commit.getDate() > commits.get(commitHash).getDate() && commit.getDate() <= commits.get(commitHash).getDate() + interval)) {
                    continue;
                }
                if (commit.isBuggy()) {
                    buggyCommits.add(commit);
                }
            }
            postReleaseDefects.put(file, buggyCommits);
        }
    }

    public void saveToFile(String outputPath) throws IOException {
        CSVWriter csvWriter = new CSVWriter();
        List<String> header = new ArrayList<>();
        header.add("FileData");
        header.add("LOC");
        header.add("InvolvedInBuggyCommits");
        header.add("PostReleaseDefects");
        header.add("APIs");
        csvWriter.initializeCSVPrinter(outputPath, header);
        for (String file : postReleaseDefects.keySet()) {
            csvWriter.writeLine(new String[]{file, String.valueOf(fileLOC.get(file)), String.join(" ",
                    postReleaseDefects.get(file).stream().map(Commit::getHash).collect(Collectors.toList())),
                    String.valueOf(postReleaseDefects.get(file).size()), String.join(" ", filesAPIs.get(file))});
        }
        csvWriter.close();
    }

    public static void run(String projectPath, String commitGuruPath, Integer interval, String releaseCommitHash, String outputPath) throws IOException, GitAPIException {
        FilePostReleaseDefectsExtractor fprde = new FilePostReleaseDefectsExtractor();
        fprde.log("loading commitGuru csv...");
        fprde.loadCommitsData(commitGuruPath);
        fprde.log("commitGuru csv loaded. Now extracting filesCommits map...");
        Map<String, List<Commit>> fileCommits = fprde.extractFilesCommitsMap(projectPath);
        fprde.log("filesCommits map extracted. Now extractPostReleaseDefects is executing...");
        fprde.extractPostReleaseDefects(releaseCommitHash, interval, fileCommits);
        fprde.log("Writing FilePostReleaseDefectsExtractor results to file");
        fprde.saveToFile(outputPath);
        fprde.log("writing FilePostReleaseDefectsExtractor results to file is Done!");
    }

}
