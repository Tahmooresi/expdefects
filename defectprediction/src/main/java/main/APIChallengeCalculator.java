package main;

import com.google.common.collect.Ordering;
import main.utils.Discussion;
import main.utils.Feature;
import main.utils.Loggable;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.util.*;

/**
 * Created by HTahmooresi on 9/29/2018.
 */
public class APIChallengeCalculator extends Loggable {


    Map<String, Double> entropies = new HashMap<>();
    Map<String, List<Discussion>> apiDiscussions = new HashMap<>();
    Map<String, Integer> apiDiscussionRelations = new HashMap<>();
    Map<Integer, Discussion> discussions = new HashMap<>();
    Map<String, Double> apiChallengeCache = new HashMap<>();
    private String apiQuestionMapDigestCSV;
    private String discussionMetricsCSV;
    private String apiEntropiesCSV;
    private int invalidDiscussion = 0;
    private int questionAPIRelationCount = 0;

    public APIChallengeCalculator(String apiEntropiesCSV, String discussionMetricsCSV, String apiPostPositionsDigestCSV) {
        this.apiEntropiesCSV = apiEntropiesCSV;
        this.discussionMetricsCSV = discussionMetricsCSV;
        this.apiQuestionMapDigestCSV = apiPostPositionsDigestCSV;

    }
    private void loadEntropies(String path) throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(path);
        for (CSVRecord csvRecord : csvParser) {
            String api = csvRecord.get(0);
            Double entropy = Double.valueOf(csvRecord.get(1));
            entropies.put(api, entropy);
        }
    }
    private void loadApiDiscussionRelations(String path) throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(path);
        for (CSVRecord csvRecord : csvParser) {
            String api = csvRecord.get(1);
            this.questionAPIRelationCount++;
            Integer discussionId = Integer.valueOf(csvRecord.get(0));
            Integer relationType = Integer.valueOf(csvRecord.get(2));
            if (shouldSkipThisDiscussion(discussionId)) {
                continue;
            }
            List<Discussion> apiDiscussionList = apiDiscussions.getOrDefault(api, new ArrayList<>());
            apiDiscussionList.add(discussions.get(discussionId));
            apiDiscussions.put(api, apiDiscussionList);
            apiDiscussionRelations.put(api + "-" + discussionId, relationType);
        }
    }

    private boolean shouldSkipThisDiscussion(Integer discussionId) {
        Discussion discussion = discussions.get(discussionId);
        if (discussion == null) {
            this.invalidDiscussion++;
            return true;
        }
        return discussion.getScore() < 1.0;
    }

    private void loadDiscussionMetrics(String path) throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(path);
        for (CSVRecord csvRecord : csvParser) {
            Discussion discussion = new Discussion();
            discussion.fillDiscussion(csvRecord);
            discussions.put(discussion.getId(), discussion);
        }
    }
    public Double getApiChallenge(String api, Feature feature){
        if (api.length() < 3){
            return 0.0;
        }
        if (apiChallengeCache.containsKey(api+"-"+feature.getName())){
            return apiChallengeCache.get(api+"-"+feature.getName());
        }
        List<Discussion> discussionsOfApi = apiDiscussions.getOrDefault(api, null);
        if (discussionsOfApi == null) {
            apiChallengeCache.put(api + "-" + feature.getName(), null);
            return null;
        }

        List<Discussion> topDiscussions = Ordering.from(feature.getComparator()).greatestOf(discussionsOfApi, 20);

        Double meanScore = 0.0;
        for (Discussion discussion : topDiscussions) {
            meanScore += discussion.getScore();
        }
        meanScore /= topDiscussions.size();
        if (entropies.containsKey(api) && entropies.get(api) != 0.0) {
            meanScore /= entropies.get(api);
        }
        apiChallengeCache.put(api + "-" + feature.getName(), meanScore);
        return meanScore;
    }
    public void preprocess() throws IOException {
        log("Loading entropies...");
        loadEntropies(apiEntropiesCSV);
        log("entropies loaded. Now loading discussion metrics");
        loadDiscussionMetrics(discussionMetricsCSV);
        log("Discussion metrics loaded. Now loading api discussion relationships...");
        loadApiDiscussionRelations(apiQuestionMapDigestCSV);
        log("api discussion relationships has been loaded! " + invalidDiscussion + " out of " + questionAPIRelationCount + " discussions were not found in discussions-metrics CSV");
    }
//    public static void main(String[] args) throws IOException {
//        APIChallengeCalculator acc = new APIChallengeCalculator("","","");
//        acc.preprocess();
//        String api = "getJspApplicationContext";
//        Double challenge = acc.getApiChallenge(api, null);
//        System.out.println(api+" Challenge is : "+challenge);
//    }
}
