package main;

import main.utils.*;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by HTahmooresi on 9/29/2018.
 */
public class JITDefectPrediction extends Loggable {

    private List<Commit> commits = new ArrayList<>();
    private static APIChallengeCalculator acc;
    private String CommitGuruPath;
    private String outputPath;
    private Map<String, List<String>> commitApis = new HashMap<>();
    private List<Feature> features;
    private String commitApiCSVPath;

    public void setCommitDotGuruPath(String path) {
        this.CommitGuruPath = path;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public void setOutputPath(String path) {
        this.outputPath = path;
    }

    public void setCommitApiCSVPath(String path) {
        this.commitApiCSVPath = path;
    }

    public void preprocess() throws IOException {
        acc.preprocess();
        log("loading commits data from commit.guru csv...");
        loadCommitsData(CommitGuruPath);
        log("loading commits api csv...");
        loadCommitsApi();
        log("All preprocessed are done!");

    }

    public void loadCommitsData(String path) throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(path);
        for (CSVRecord csvRecord : csvParser) {
            Commit commit = new Commit();
            commit.setData(csvRecord);
            commits.add(commit);
        }
    }

    public void loadCommitsApi() throws IOException {
        CSVParser csvParser = CSVReader.getCSVParser(commitApiCSVPath);
        int overall = 0;
        int nullObjects = 0;
        for (CSVRecord csvRecord : csvParser) {
            String hash = csvRecord.get(0);
            List<String> apis = Arrays.asList(csvRecord.get(1).split("\\s+"));
            Commit commit = getCommitByHash(hash);
            if (commit != null) {
                commit.setApis(apis);
            } else {
                nullObjects++;
            }
            overall++;
        }
        log(nullObjects + " of " + overall + " commits where not found in commit.guru csv");
    }

    private Commit getCommitByHash(String hash) {
        for (Commit commit : commits) {
            if (commit.getHash().equals(hash)) {
                return commit;
            }
        }
        return null;
    }

    public void calculateFeatures() throws IOException, GitAPIException {
        int counter = 0;
        for (Commit commit : commits) {
//            counter ++;
//            if (counter % 1000 == 0){
//                log("All features for "+counter+" commits calculated");
//            }
            for (Feature feature : features) {
                calculateFeatureForCommit(commit, feature);
            }
        }
    }

    public void calculateFeatureForCommit(Commit commit, Feature feature) {
        Double maxChallegingAPI = 0.0;
        for (String api : commit.getApis()) {
            Double challenge = acc.getApiChallenge(api, feature);
            if (challenge == null) {
                continue;
            }
            maxChallegingAPI = Math.max(maxChallegingAPI, challenge);
        }

        commit.setFeature(feature, maxChallegingAPI);

    }

    public void writeResultsToFile() throws IOException {
        log("Writing results to file");
        CSVWriter csvWriter = new CSVWriter();
        csvWriter.initializeCSVPrinter(outputPath, getHeader());
        int skippedCommitCount = 0;
        for (Commit commit : commits) {
            List<String> line = commit.getData();
            boolean skipCommit = false;
            for (Feature feature : commit.getFeatures()) {
                Double featureValue = commit.getFeature(feature);
                if (featureValue.isNaN() || featureValue.isInfinite()) {
                    skipCommit = true;
                }
                line.add(String.valueOf(featureValue));
            }
            if (skipCommit) {
                skippedCommitCount++;
                continue;
            }
            String[] arrayLine = new String[line.size()];
            csvWriter.writeLine(line.toArray(arrayLine));
        }
        csvWriter.close();
        log("writing results to file is Done! we've encountered errors calculating features for " + skippedCommitCount + " commits.");
    }

    private List<String> getHeader() {
        List<String> header = Commit.getHeader();
        List<String> featuresName = features.stream().map(Feature::getName).collect(Collectors.toList());
        header.addAll(featuresName);
        return header;
    }

    public static void run(List<Feature> features, String guruProjectPath, String commitApiCSVPath, String apiEntropiesCSV, String outputPath, String discussionMetricsCSV, String apiPostPositionsDigestCSV) throws IOException, GitAPIException {
        acc = new APIChallengeCalculator(apiEntropiesCSV, discussionMetricsCSV, apiPostPositionsDigestCSV);
        JITDefectPrediction jdf = new JITDefectPrediction();
        jdf.setCommitDotGuruPath(guruProjectPath);
        jdf.setOutputPath(outputPath);
        jdf.setCommitApiCSVPath(commitApiCSVPath);
        jdf.setFeatures(features);
        jdf.preprocess();
        jdf.calculateFeatures();
        jdf.writeResultsToFile();

    }
//    public static void main(String[] args) throws IOException, GitAPIException {
//        JITDefectPrediction.run(null, "C:\\Research\\Dataset\\commit.guru\\AmazeFileManager.csv", "C:\\Research\\Dataset\\commit.guru\\extracted\\AmazeFileManager-commit-apis.csv", "","AmazeFileManager-output.csv", "", "");
//    }
}
