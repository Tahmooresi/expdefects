package main;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by HTahmooresi on 9/29/2018.
 */
public class CSVReader {
    public static CSVParser getCSVParser(String path) throws IOException {
        FileReader filereader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(filereader);
        bufferedReader.readLine();
        return new CSVParser(bufferedReader, CSVFormat.DEFAULT);
    }
}
