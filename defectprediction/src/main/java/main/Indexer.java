package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;
import utility.StaticSharedData;

public class Indexer {

    private static HashMap<String, Double> entropy = new HashMap<>();
    private static HashMap<String, ArrayList<Integer>> numInOneDiscussion = new HashMap<>();
    private static HashMap<String, Integer> numInAllDiscussions = new HashMap<>();
    private static ArrayList<String> allClasses = new ArrayList<>();
    private static ArrayList<String> allMethods = new ArrayList<>();
    private static int numberOfAllDiscussions = 0;

    public static void main(String[] args) throws FileNotFoundException, IOException, JSONException {

        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        String indexName = "index";
        Directory directory = new SimpleFSDirectory(Paths.get(indexName));
        IndexWriter writer = new IndexWriter(directory, config);

        File jsons = new File(StaticSharedData.STORMED_DATASET_PATH);
        File[] jsonFiles = jsons.listFiles();
//		System.out.println("Indexing started");
        assert jsonFiles != null;
        for (File jsonFile : jsonFiles) {
            Document doc = new Document();

            if (jsonFile.isFile()) {
                try {
                    allClasses = new ArrayList<>();
                    allMethods = new ArrayList<>();

                    JSONObject json = null;
                    try {
                        json = new JSONObject(new JSONTokener(new InputStreamReader(new FileInputStream(jsonFile))));
                    } catch (Exception e) {
                        continue;
                    }
                    if (numberOfAllDiscussions % 1000 == 0) {
                        System.out.println(numberOfAllDiscussions);
                    }
                    //unit_id
                    // $.question.id
                    int id = JsonPath.read(json.toString(), "$.question.id");

                    Object output = JsonPath.read(json.toString(), "$.question.informationUnits[?(@.type=='CodeTaggedUnit')]");
                    JSONArray array = (JSONArray) output;
                    for (int i = 0; i < array.size(); i++) {
                        LinkedHashMap<Object, Object> map = (LinkedHashMap<Object, Object>) array.get(i);
                        String codeId = JsonPath.read(array.get(i), "$.id");
                        doc.add(new StringField("unit_id", codeId, Store.YES));
                        indexAnnotations(doc, map);
                        indexClassDeclaration(doc, map);
                        indexMethodDeclarations(doc, map);
                        indexImport(doc, map);
                        indexClassUsage(doc, map);
                        indexMethodDeclarations(doc, map);

                        writer.addDocument(doc);
                    }

                    indexText(doc, json);
                    indexTitle(doc, json);

                    updateNumInDiscussions();
                    numberOfAllDiscussions++;
                } catch (Exception e) {
//					e.printStackTrace(System.out);
                }
            }
        }
        printEntropies();
        writer.close();
        System.out.println("finished");
    }

    /**
     * raw code
     * $.question.informationUnits[?(@.type=='CodeTaggedUnit')]
     * .rawText
     **/
    private static void indexRawCode(Document doc, LinkedHashMap<Object, Object> json) {
        String rawText = JsonPath.read(json,
                "$.rawText");
        addToDoc(doc, rawText, "RAW");
    }

    /**
     * Annotations
     * $.question.informationUnits[?(@.type=='CodeTaggedUnit')]
     * .astNode..[?(@.type=='AnnotationNode')].identifier..name
     **/
    private static void indexAnnotations(Document doc, LinkedHashMap<Object, Object> json) {
        List<String> annotations = JsonPath.read(json,
                "$.astNode..[?(@.type=='AnnotationNode')].identifier..name");

        addListToAllTerms(annotations);
        addListToOneDiscussionTerms(annotations);
        addToDoc(doc, annotations, "ANNOTATION");
    }

    /**
     * Class Declaration
     * $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='ClassDeclarationNode')].identifier.name
     **/
    private static void indexClassDeclaration(Document doc, LinkedHashMap<Object, Object> json) {
        List<String> classDeclarations = JsonPath.read(json,
                "$.astNode..[?(@.type=='ClassDeclarationNode')].identifier.name");

        addListToAllTerms(classDeclarations);
        addListToOneDiscussionTerms(classDeclarations);
        addToAllClasses(classDeclarations);
        addToDoc(doc, classDeclarations, "CLASS");
    }

    /**
     * Method Invocation // just name of method - System.out.println() -> just println
     * $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='MethodInvocationNode')].identifier.name
     **/
    private static void indexMethodInvocation(Document doc, LinkedHashMap<Object, Object> json) {
        List<String> methodInvocations = JsonPath.read(json,
                "$.astNode..[?(@.type=='MethodInvocationNode')].identifier.name");

        addListToAllTerms(methodInvocations);
        addListToOneDiscussionTerms(methodInvocations);
        addToAllMethods(methodInvocations);
        addToDoc(doc, methodInvocations, "METHOD");
    }

    /**
     * Imports
     * $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='ImportDeclarationNode')].identifier..name
     **/
    private static void indexImport(Document doc, LinkedHashMap<Object, Object> json) {
        List<String> imports = JsonPath.read(json,
                "$.astNode..[?(@.type=='ImportDeclarationNode')].identifier..name");

        addListToAllTerms(imports);
        addListToOneDiscussionTerms(imports);
        addToAllClasses(imports);
        addToDoc(doc, imports, "IMPORTS");
    }

    /**
     * Class Usage ( parameterizedtypenode ?)
     * $.question.informationUnits[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='ParameterizedTypeNode')].identifier.name
     * $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='CatchTypeNode')]..name
     **/
    private static void indexClassUsage(Document doc, LinkedHashMap<Object, Object> json) {
        List<String> usages = JsonPath.read(json,
                "$.astNode..[?(@.type=='ParameterizedTypeNode')].identifier.name");
        List<String> catches = JsonPath.read(json,
                "$.astNode..[?(@.type=='CatchTypeNode')]..name");

        addListToAllTerms(usages);
        addListToAllTerms(catches);
        addListToOneDiscussionTerms(usages);
        addListToOneDiscussionTerms(catches);
        addToAllClasses(usages);
        addToAllClasses(catches);
        addToDoc(doc, usages, "CLASS");
        addToDoc(doc, catches, "CLASS");
    }

    /**
     * Method Declaration
     * $.question.informationUnits.[?(@.type=='CodeTaggedUnit')].
     * astNode..[?(@.type=='MethodDeclarationNode')].identifier.name
     **/
    private static void indexMethodDeclarations(Document doc, LinkedHashMap<Object, Object> json) {
        List<String> methods = JsonPath.read(json,
                "$.astNode..[?(@.type=='MethodDeclarationNode')].identifier.name");

        addListToAllTerms(methods);
        addListToOneDiscussionTerms(methods);
        addToAllMethods(methods);
        addToDoc(doc, methods, "METHOD");
    }

    private static void indexTitle(Document doc, JSONObject json) {
        String title = JsonPath.read(json.toString(), "$.question.title");
        HashMap<String, Integer> classResults = searchListInText(title, allClasses);
        HashMap<String, Integer> mResults = searchListInText(title, allMethods);

        addListToAllTerms(classResults.keySet());
        addListToAllTerms(mResults.keySet());
        addListToOneDiscussionTerms(classResults.keySet());
        addListToOneDiscussionTerms(mResults.keySet());
        //TODO distinct code search, no use of these
//		addToDoc(doc, classResults.keySet(), "TITLE_CLASS");
//		addToDoc(doc, mResults.keySet(), "TITLE_METHOD");
    }

    private static void indexText(Document doc, JSONObject json) {
        List<String> texts = JsonPath.read(json.toString(), "$.question.informationUnits."
                + "[?(@.type=='NaturalLanguageTaggedUnit')].rawText");

        for (String text : texts) {
            HashMap<String, Integer> classResults = searchListInText(text, allClasses);
            HashMap<String, Integer> mResults = searchListInText(text, allMethods);

            addListToAllTerms(classResults.keySet());
            addListToAllTerms(mResults.keySet());
            addListToOneDiscussionTerms(classResults.keySet());
            addListToOneDiscussionTerms(mResults.keySet());

            //TODO distinct code search, no use of these
//			addToDoc(doc, classResults.keySet(), "TEXT_CLASS");
//			addToDoc(doc, mResults.keySet(), "TEXT_METHOD");
        }
    }

    private static void addToDoc(Document doc, List<String> list, String name) {
        for (String string : list) {
            doc.add(new StringField(name, string, Store.YES));
        }
    }

    private static void addToDoc(Document doc, String string, String name) {
        doc.add(new StringField(name, string, Store.YES));
    }

    private static void addToDoc(Document doc, Set<String> set, String name) {
        for (String string : set) {
            doc.add(new StringField(name, string, Store.YES));
        }
    }

    private static void addListToOneDiscussionTerms(List<String> list) {
        for (String term : list) {
            addOneToDiscussionTerms(term);
        }
    }

    private static void addListToOneDiscussionTerms(Set<String> set) {
        for (String term : set) {
            addOneToDiscussionTerms(term);
        }
    }

    private static void addListToAllTerms(List<String> list) {
        for (String term : list) {
            addOneToAllTerms(term);
        }
    }

    private static void addListToAllTerms(Set<String> set) {
        for (String term : set) {
            addOneToAllTerms(term);
        }
    }

    private static void addOneToDiscussionTerms(String key) {
        if (numInOneDiscussion == null)
            return;
        else if (!numInOneDiscussion.containsKey(key)) {
            ArrayList<Integer> nums = new ArrayList<>();
            nums.add(1);
            numInOneDiscussion.put(key, nums);
        } else {
            ArrayList<Integer> nums = numInOneDiscussion.get(key);
            nums.set(nums.size() - 1, nums.get(nums.size() - 1) + 1);
        }
    }

    private static void addOneToAllTerms(String key) {
        if (numInAllDiscussions == null)
            return;
        else if (!numInAllDiscussions.containsKey(key))
            numInAllDiscussions.put(key, 1);
        else
            numInAllDiscussions.put(key, numInAllDiscussions.get(key) + 1);
    }

    private static void updateNumInDiscussions() {
        if (numInOneDiscussion == null) {
            return;
        }

        // vaghti in discussion tamum shod, bayad yek arraylist ba sefr tahesh bashe ,
        // vaarna dafe dige
        // be akhari yeki ezafe mikone va mohasebat ghalat mishe
        for (String key : numInOneDiscussion.keySet()) {
            ArrayList<Integer> nums = numInOneDiscussion.get(key);
            if (nums.get(nums.size() - 1).equals(0))
                continue;
            nums.add(0);
        }
    }

    private static void calculateEntropies() {
        for (String key : numInAllDiscussions.keySet()) {
            try {
                double sum = 0;
                int allNum = numInAllDiscussions.get(key);
                ArrayList<Integer> nums = numInOneDiscussion.get(key);
                for (Integer i : nums) {
                    double prob = (double) i / (double) allNum;
                    if (prob != 0.0) {
                        sum += prob * (-1) * (Math.log(prob) / Math.log(numberOfAllDiscussions));
                    }
                }
                entropy.put(key, sum);
            } catch (Exception e) {
//				e.printStackTrace(System.out);
            }
        }
    }

    private static void printEntropies() throws FileNotFoundException {
        calculateEntropies();
        System.out.println("entropies calculated");
        FileOutputStream f = new FileOutputStream(StaticSharedData.ENTROPIES);
        System.out.println("file created");
        PrintStream p = new PrintStream(f);
        for (String key : entropy.keySet()) {
            try {
                p.println(key + ":" + String.valueOf(entropy.get(key)));
            } catch (Exception e) {
//				e.printStackTrace(System.out);
                System.out.println("here");
            }
        }
        p.flush();
        p.close();
    }

    private static void addToAllClasses(List<String> key) {
        for (String c : key) {
            if (!allClasses.contains(c)) {
                allClasses.add(c);
            }
        }
    }

    private static void addToAllMethods(List<String> key) {
        for (String m : key) {
            if (!allMethods.contains(m)) {
                allMethods.add(m);
            }
        }
    }

    private static HashMap<String, Integer> searchListInText(String text, List<String> list) {
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        for (String term : list) {
            int matches = StringUtils.countMatches(term.toLowerCase(), text.toLowerCase());
            if (matches > 0) {
                result.put(term, matches);
            }
        }
        return result;
    }
}


//Class Extension ?! 
//hamun parameterizedTypeNode javab mide !

