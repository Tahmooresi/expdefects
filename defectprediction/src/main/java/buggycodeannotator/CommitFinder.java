package buggycodeannotator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

import main.utils.Loggable;
import org.apache.commons.text.similarity.CosineSimilarity;
import org.apache.lucene.queryparser.classic.ParseException;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.blame.BlameResult;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.Edit;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.jayway.jsonpath.JsonPath;

import main.Search;
import utility.StaticSharedData;

/**
 * Created by smmsadrnezh on 11/30/16.
 */

public class CommitFinder extends Loggable {
    private Repository repository;
    private String path;
    private Git git;
    private HashMap<RevCommit, String> allCommitsData;
    private Map<String, List<String>> commitAPIs = new HashMap<>();
    private Map<String, List<String>> deletedCommitAPIs = new HashMap<>();
    private ArrayList<String> bugTerms = new ArrayList<String>() {
        {
            add("bugfix");
            add("fix");
            add("bug");
            add("issue");
            add("resolve");
        }
    };

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    private HashMap<RevCommit, Integer> numberOfFilesOfCommit = new HashMap<>();
    private HashMap<ObjectId, HashMap> result = new HashMap<>();

    public CommitFinder(String path) {
        this.path = path;
    }

    public Repository initializeRepository() throws IOException {
        /**
         * Using the provided path, this function initialize necessary variables,
         * including `repository` and `git` objects
         * @return Repository:
         * repository
         */
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        repository = builder.setGitDir(new File(path)).readEnvironment().findGitDir().build();
        git = new Git(repository);
        return repository;
    }

    public ArrayList<RevCommit> findAllCommits() throws IOException, GitAPIException {
        /**
         * Iterates over all of the commits in the repository using the `git` object
         * @return ArrayList:
         * RevCommit
         */
        log("Gathering all commits");
        Iterable<RevCommit> allCommits;

        allCommits = git.log().all().call();

        ArrayList<RevCommit> result = new ArrayList<>();

        for (RevCommit commit : allCommits) {
            result.add(commit);
        }
        log("All Commits gathered");
        return result;

    }

    private Boolean isBugfixCommit(RevCommit commit) {
        /**
         * Determines if a given commit is buggy by simply looking for certain
         * `bugTerm` words in the commit message
         * @param RevCommit:
         *              RevCommit object to determine
         * @return Boolean:
         *              buggy or not
         */

        for (String bugTerm : bugTerms) {
            if (commit.getShortMessage().toLowerCase().contains(bugTerm)) {
                return true;
            }
        }
        return false;
    }

//    public void annotateCommits(ArrayList<RevCommit> allCommits, HashMap<RevCommit, String> allCommitsData)
//            throws GitAPIException, IOException, ParseException {
//        this.allCommitsData = allCommitsData;
//        int a = 0;
//        for (RevCommit commit : allCommits) {
//            a++;
////			if (a < 100) {
////				a++;
////			} else
////				break;
////			System.out.println("fix : " + bugfixCommit.toString());
////			System.out.println(a);
//
//            DiffManager diffManager = new DiffManager(repository);
//
//            try {
//                checkoutOneCommitBeforeBugfix(commit);
//            } catch (ArrayIndexOutOfBoundsException e) {
////				e.printStackTrace(System.out);
//                continue;
//            }
//
//            /** Find MODIFY edits */
//            for (DiffEntry diffEntry : diffManager.getDiffEntries(commit)) {
//                if (diffEntry.getChangeType().toString().equals("ADD")) {
//                    String changedFilePath = diffEntry.getPath(DiffEntry.Side.NEW);
//                    if (!changedFilePath.endsWith("java")) {
//                        continue;
//                    }
//
//                    ArrayList<Integer> lineNumbers = new ArrayList<>();
//                    // TODO: This is weird, but working for now!
//                    for (int i = 0; i < 100000; i++) {
//                        lineNumbers.add(i);
//                    }
//
//                    try {
//                        getScores(commit, changedFilePath, lineNumbers, false);
//                    } catch (Exception e) {
////						e.printStackTrace(System.out);
//                        continue;
//                    }
//                    // TODO: Don't know what to do with score here.
//
//
//                } else if (diffEntry.getChangeType().toString().equals("MODIFY")) {
//
//                    String changedFilePath = diffEntry.getPath(DiffEntry.Side.NEW);
//                    if (!changedFilePath.endsWith("java")) {
//                        continue;
//                    }
//                    BlameResult fileBlameResult = annotateFile(changedFilePath);
//
//                    ArrayList<Integer> allEditedLineNumbers = new ArrayList<Integer>();
//                    ArrayList<Integer> allDeletedLineNumbers = new ArrayList<Integer>();
//                    for (Edit edit : diffManager.getEdits(diffEntry)) {
//
//                        ArrayList<Integer> commitDeletedLineNumbers = getCommitDeletedLineNumbers(edit);
//                        ArrayList<Integer> commitAddedLineNumbers = getCommitAddedLineNumbers(edit);
//                        // TODO: with deleted line numbers, the older version of code should be passed to ast parser
////                        addToAllLines(commitDeletedLineNumbers, allEditedLineNumbers);
//                        addToAllLines(commitDeletedLineNumbers, allDeletedLineNumbers);
//                        addToAllLines(commitAddedLineNumbers, allEditedLineNumbers);
//
//                    }
//                    try {
//                        getScores(commit, changedFilePath, allEditedLineNumbers, false);
//                    } catch (Exception e) {
////						e.printStackTrace(System.out);
//                        continue;
//                    }
//
//                    if (isBugfixCommit(commit)) {
//                        buildResult(allDeletedLineNumbers, fileBlameResult, commit);
//                        HashMap<RevCommit, ArrayList> annotateCommits = result.get(commit.getId());
//                        for (RevCommit buggyCommit : annotateCommits.keySet()) {
//                            try {
//                                getScores(buggyCommit, changedFilePath, annotateCommits.get(buggyCommit), true);
//                            } catch (Exception e) {
//                                System.err.println(e.getMessage());
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

    private ArrayList<Integer> getCommitAddedLineNumbers(Edit edit) {
        /**
         * For a given `edit` object associated to a commit, return a list
         * of added line numbers
         * @param Edit:
         *            edit
         * @return ArrayList:
         *            addedLineNumbers
         *
         */
        ArrayList<Integer> deletedLines = new ArrayList();
        for (int line = edit.getBeginB() + 1; line <= edit.getEndB(); line++)
            deletedLines.add(line);
        return deletedLines;
    }

    private ArrayList<Integer> getCommitDeletedLineNumbers(Edit edit) {
        /**
         * For a given `edit` object associated to a commit, return a list
         * of deleted line numbers
         * @param Edit:
         *            edit
         * @return ArrayList:
         *            deletedLineNumbers
         *
         */
        ArrayList<Integer> addedLines = new ArrayList();
        for (int line = edit.getBeginA() + 1; line <= edit.getEndA(); line++)
            addedLines.add(line);
        return addedLines;
    }

    private void checkoutOneCommitBeforeBugfix(RevCommit bugfixCommit) throws GitAPIException {
        git.checkout().setName(bugfixCommit.getParent(0).getName()).call();
    }

    public String getCode(RevCommit commit, String filePath) throws IOException {
        /**
         * for a given commit and filePath, return the associated code for
         * the specific file in the specific commit
         * @param RevCommit:
         *                 commit
         * @param String:
         *              filePath
         * @return String:
         * the code itself
         *
         */
//        ObjectId commitId = commit.getId();

        try (RevWalk revWalk = new RevWalk(repository)) {
//            RevCommit commit = revWalk.parseCommit(commitId);
            RevTree tree = commit.getTree();

            try (TreeWalk treeWalk = new TreeWalk(repository)) {
                treeWalk.addTree(tree);
                treeWalk.setRecursive(true);
                treeWalk.setFilter(PathFilter.create(filePath));
                if (!treeWalk.next()) {
                    throw new IllegalStateException("Did not find expected file");
                }

                ObjectId objectId = treeWalk.getObjectId(0);
                ObjectLoader loader = repository.open(objectId);

                OutputStream output = new OutputStream() {
                    private StringBuilder string = new StringBuilder();

                    @Override
                    public void write(int b) throws IOException {
                        this.string.append((char) b);
                    }

                    public String toString() {
                        return this.string.toString();
                    }
                };

                loader.copyTo(output);
                revWalk.dispose();
                output.close();
                treeWalk.close();
                return output.toString();
            }
        }
    }

    private void buildResult(ArrayList<Integer> bugfixCommitDeletedLineNumbers, BlameResult fileBlameResult,
                             RevCommit bugfixCommit) throws GitAPIException, IOException {
        /** find annotate commits for each line */
        HashMap<RevCommit, ArrayList<Integer>> annotateCommits = new HashMap<>();
        if (result.containsKey(bugfixCommit.getId())) {
            annotateCommits = result.get(bugfixCommit.getId());
        }
        RevCommit buggyCommit;
        for (Integer bugfixCommitDeletedLineNumber : bugfixCommitDeletedLineNumbers) {
            buggyCommit = annotateLine(fileBlameResult, bugfixCommitDeletedLineNumber);
            if (!annotateCommits.containsKey(buggyCommit)) {
                ArrayList<Integer> deletedLineNumbers = new ArrayList();
                deletedLineNumbers.add(getSourceLineNumber(fileBlameResult, bugfixCommitDeletedLineNumber));
                annotateCommits.put(buggyCommit, deletedLineNumbers);
            } else {
                annotateCommits.get(buggyCommit)
                        .add(getSourceLineNumber(fileBlameResult, bugfixCommitDeletedLineNumber));
            }
        }
        result.put(bugfixCommit.getId(), annotateCommits);
    }

    private BlameResult annotateFile(String changedFilePath) throws GitAPIException {
        return git.blame().setFilePath(changedFilePath).call();
    }

    private RevCommit annotateLine(BlameResult fileBlameResult, int lineNumber) throws GitAPIException, IOException {
        return fileBlameResult.getSourceCommit(lineNumber - 1);
    }

    private int getSourceLineNumber(BlameResult fileBlameResult, int lineNumber) {
        return fileBlameResult.getSourceLine(lineNumber - 1) + 1;
    }

//    private void getScores(RevCommit commit, String changedFilePath, ArrayList<Integer> changedLines, boolean buggy)
//            throws IOException, ParseException {
//        astParser = new AbstractSyntaxTreeCrawler();
//
//        String afterBuggyCommitCode = getCode(commit, changedFilePath);
//        astParser.buildAST(afterBuggyCommitCode, changedLines);
//
//        double postScore = calculatePostScoreForCommit(Search.search(astParser.getQuery()));
//
//        double apiScore = calculateAPIScoreForCommit(astParser.getAllAPIs());
//        double classScore = calculateAPIScoreForCommit(astParser.getAllClasses());
//        double methodDeclarationScore = calculateAPIScoreForCommit(astParser.getAllMethodDeclarations());
//        double methodInvocationsScore = calculateAPIScoreForCommit(astParser.getAllMethodInvocations());
//        double annotationScore = calculateAPIScoreForCommit(astParser.getAllAnnotations());
//
//        if (this.numberOfFilesOfCommit.containsKey(commit))
//            this.numberOfFilesOfCommit.put(commit, this.numberOfFilesOfCommit.get(commit).intValue() + 1);
//        else
//            this.numberOfFilesOfCommit.put(commit, 1);
//
//        addToAllCommitsData(commit, classScore, methodDeclarationScore, methodInvocationsScore, annotationScore,
//                postScore, changedLines.size(), buggy);
//    }
//
    private void addToAllLines(ArrayList<Integer> editedLines, ArrayList<Integer> allLines) {
        for (Integer line : editedLines) {
            if (!allLines.contains(line)) {
                allLines.add(line);
            }
        }
    }

//    private double calculatePostScoreForCommit(HashMap<String, Float> luceneScores) {
//        double result = 0.0;
//        for (String unit : luceneScores.keySet()) {
//            try {
//                String postId = unit.split("-")[0];
//                double postScore = StaticSharedData.postScores.get(postId);
////				System.out.println(isBuggyPost(postId));
//                result += (postScore * luceneScores.get(unit).doubleValue());
//            } catch (Exception e) {
//                // TODO tedade ziadi az post ha tooye postScores nistan :/
////				e.printStackTrace(System.out);
//                continue;
//            }
//        }
//        return result;
//    }

//    private boolean isBuggyPost(String postId) {
//        double threshold = 1.0;
//        File jsonFile = new File("/home/tahmooresi/stormed-dataset/" + postId);
//        JSONObject json = null;
//
//        try {
//            json = new JSONObject(new JSONTokener(new InputStreamReader(new FileInputStream(jsonFile))));
//            List<String> questions = JsonPath.read(json.toString(),
//                    "$.question.informationUnits[?(@.type=='CodeTaggedUnit')].rawText");
//            List<String> answers = JsonPath.read(json.toString(),
//                    "$.answers[?(@.isAccepted==true)].." + "informationUnits[?(@.type=='CodeTaggedUnit')].rawText");
//            for (int i = 0; i < questions.size(); i++) {
//                for (int j = 0; j < answers.size(); j++) {
//                    CosineSimilarity dist = new CosineSimilarity();
//
//                    String c1 = questions.get(i);
//                    String c2 = answers.get(j);
//
//                    Map<CharSequence, Integer> leftVector = Arrays.stream(c1.split(""))
//                            .collect(Collectors.toMap(c -> c, c -> 1, Integer::sum));
//                    Map<CharSequence, Integer> rightVector = Arrays.stream(c2.split(""))
//                            .collect(Collectors.toMap(c -> c, c -> 1, Integer::sum));
//
////					System.out.println(1 - dist.cosineSimilarity(leftVector, rightVector));
//                    if (1 - dist.cosineSimilarity(leftVector, rightVector) > threshold)
//                        return true;
//                }
//            }
//        } catch (Exception e) {
////			e.printStackTrace(System.out);
//        }
//
//        return false;
//    }

    private double calculateAPIScoreForCommit(ArrayList<String> apis) {
        double result = 0.0;
        for (String api : apis) {
            try {
                result += StaticSharedData.apiScores.get(api).doubleValue();
            } catch (Exception e) {
//				e.printStackTrace(System.out);g
                continue;
            }
        }
        return result;
    }

    private void addToAllCommitsData(RevCommit commit, double classScore, double methodDeclarationScore,
                                     double methodInvocationScore, double annotationScore, double postScore, int changedLinesNumber,
                                     boolean buggy) {
        if (allCommitsData == null)
            allCommitsData = new HashMap<RevCommit, String>();
        else if (allCommitsData.containsKey(commit)) {
            // miangin : data ro migire, zarbdar tedad mikone + data jadid mikone taghsim bar tedad

            int tedad = this.numberOfFilesOfCommit.get(commit).intValue();
            String commitData = allCommitsData.get(commit);
            String[] splits = commitData.split(",");
            classScore = (classScore + (Double.parseDouble(splits[1]) * tedad - 1)) / tedad;
            methodDeclarationScore = (methodDeclarationScore + (Double.parseDouble(splits[2]) * tedad - 1)) / tedad;
            methodInvocationScore = (methodInvocationScore + (Double.parseDouble(splits[3]) * tedad - 1)) / tedad;
            annotationScore = (annotationScore + (Double.parseDouble(splits[4]) * tedad - 1)) / tedad;
            postScore = (postScore + (Double.parseDouble(splits[5]) * tedad - 1)) / tedad;
            if (postScore < 0) {
                System.out.println("POSTSCORE LESS THAN 0:" + " " + postScore);
                postScore = Math.max(0, postScore);
            }
            String value = commit.getName().toString() + "," + String.valueOf(classScore) + ","
                    + String.valueOf(methodDeclarationScore) + "," + String.valueOf(methodInvocationScore) + ","
                    + String.valueOf(annotationScore) + "," + String.valueOf(postScore) + ","
                    + String.valueOf(changedLinesNumber) + "," + String.valueOf(buggy);
            allCommitsData.put(commit, value);
        } else {
            String value = commit.getName().toString() + "," + String.valueOf(classScore) + ","
                    + String.valueOf(methodDeclarationScore) + "," + String.valueOf(methodInvocationScore) + ","
                    + String.valueOf(annotationScore) + "," + String.valueOf(postScore) + ","
                    + String.valueOf(changedLinesNumber) + "," + String.valueOf(buggy);
            allCommitsData.put(commit, value);
        }
    }

    public Map<String, List<String>> extractCommitPresentAPIsByFile(RevCommit commit) throws IOException {
        /**
         * For a given commit, creates a map of filePath to APIs presented in the file (java files)
         * @param RevCommit:
         *                 commit to extract the info
         * @return Map:
         * String (path) --> List<String> (APIs)
         */
        Map<String, List<String>> filePathToAPIs = new HashMap<>();

        Set<String> javaFilesInCommit = new LinkedHashSet<>();
        TreeWalk treeWalk = new TreeWalk(repository);
        treeWalk.addTree(commit.getTree());
        treeWalk.setRecursive(true);
        while (treeWalk.next()) {
            String path = treeWalk.getPathString();
            if (path.endsWith(".java"))
                javaFilesInCommit.add(path);
        }
        treeWalk.close();
        ArrayList<Integer> lineNumbers = new ArrayList<>();

        // TODO: This is weird, but working for now!
        for (int i = 0; i < 100000; i++) {
            lineNumbers.add(i);
        }

        for (String path : javaFilesInCommit) {
            String code = getCode(commit, path);
            AbstractSyntaxTreeCrawler astParser = new AbstractSyntaxTreeCrawler();
            astParser.buildAST(code, lineNumbers);
            List<String> apis = new ArrayList<>();
            for (String api : astParser.getAllAPIs()) {
                apis.add(api.split("\\.")[api.split("\\.").length - 1]);
            }
            filePathToAPIs.put(path, apis);
        }
        return filePathToAPIs;
    }

    public List<String> getChangedFiles(RevCommit commit) throws IOException {
        /**
         * For a given commit, return a list of filePaths associated to the changed (java) files
         * @param RevCommit:
         *                 commit
         * @return List<String>:
         * ArrayList of changed files
         */
        List<String> changedFilePaths = new ArrayList<>();
        DiffManager diffManager = new DiffManager(repository);
        /** Find MODIFY edits */
        for (DiffEntry diffEntry : diffManager.getDiffEntries(commit)) {
            if (diffEntry.getChangeType().toString().equals("MODIFY")) {

                String changedFilePath = diffEntry.getPath(DiffEntry.Side.NEW);
                if (!changedFilePath.endsWith(".java")) {
                    continue;
                }
                changedFilePaths.add(changedFilePath);
            }
        }

        return changedFilePaths;
    }

    public void extractCommitAPIs()
            throws GitAPIException, IOException {
        /**
         * For all of the commits in the repository extract the APIs of CHANGED LINES.
         * For deleted lines, it stores the result in `deletedCommitAPIs`
         * For added or modified lines, it stores the result in `commitAPIs`
         */

        List<RevCommit> allCommits = findAllCommits();
        int overallCommitsCount = allCommits.size();
        int counter = 0;
        int failedCommits = 0;
        for (RevCommit commit : allCommits) {
            String commitHash = commit.getName();
//            if (!commitHash.equals("f3a925fdbec7b336e0501118880630a619625cad")){
//                continue;
//            }
            try {
                counter++;
                if (counter % 100 == 0) {
                    log(counter + " commits processed inside extractCommitAPIs");
                }
                DiffManager diffManager = getDiffManager(commit);

                /** Find MODIFY edits */
                for (DiffEntry diffEntry : diffManager.getDiffEntries(commit)) {
                    extractAPIsFromDiff(commit, diffManager, diffEntry);
                }
            } catch (Exception e) {
                log("Error occured in commit : " + commit.getId());
                e.printStackTrace();
                failedCommits++;
            }

        }
        log(failedCommits + " out of " + overallCommitsCount + " commits encountered error!");
    }

    private DiffManager getDiffManager(RevCommit commit) throws IOException {
        /**
         * Create and return a DiffManager for a given commit to extract changes
         * @param RevCommit:
         *                 commit to extract data
         * @return DiffManager:
         * diffManager
         */
        ObjectReader reader = getRepository().newObjectReader();
        CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
        ObjectId oldTree = getRepository().resolve( commit.getName()+"^{tree}" ); // equals newCommit.getTree()
        oldTreeIter.reset( reader, oldTree );
        CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
        ObjectId newTree = getRepository().resolve( commit.getName()+"~1^{tree}" ); // equals oldCommit.getTree()
        newTreeIter.reset( reader, newTree );

        return new DiffManager(getRepository());
    }

    private void extractAPIsFromDiff(RevCommit commit, DiffManager diffManager, DiffEntry diffEntry) throws IOException {
        /**
         * For a given commit and diffManager, extracts APIs associated with the
         * DELETED and MODIFIED(Add or Change) lines
         * @param RevCommit:
         *                 commit
         * @param DiffManager:
         *                   diffManager acquired from `getDiffManager()`
         * @param DiffEntry:
         *                 diffEntry acquired from `DiffManager` - loop for every diffEntry
         */
        String changedFilePath = diffEntry.getPath(DiffEntry.Side.NEW);
        if (!changedFilePath.endsWith("java"))
            return;
        if (diffEntry.getChangeType().toString().equals("ADD")) {
            extractAPIsForAddedFiles(commit, diffEntry);
        } else if (diffEntry.getChangeType().toString().equals("MODIFY")) {
            extractAPIsForModifiedFiles(commit, diffManager, diffEntry);
        }
    }

    private void extractAPIsForModifiedFiles(RevCommit commit, DiffManager diffManager, DiffEntry diffEntry) throws IOException {
        /**
         * Extracts APIs associated with deleted or edited lines of a given commit
         */

        String changedFilePath = diffEntry.getPath(DiffEntry.Side.NEW);

        ArrayList<Integer> allEditedLineNumbers = new ArrayList<>();
        ArrayList<Integer> allDeletedLineNumbers = new ArrayList<>();

        for (Edit edit : diffManager.getEdits(diffEntry)) {
            ArrayList<Integer> commitDeletedLineNumbers = getCommitDeletedLineNumbers(edit);
            ArrayList<Integer> commitAddedLineNumbers = getCommitAddedLineNumbers(edit);
            addToAllLines(commitDeletedLineNumbers, allDeletedLineNumbers);
            addToAllLines(commitAddedLineNumbers, allEditedLineNumbers);
        }
        if (!allDeletedLineNumbers.isEmpty())
            extractAPIsForDeletedLines(commit, changedFilePath, allDeletedLineNumbers);
        if (!allEditedLineNumbers.isEmpty())
            extractAPIsForModifiedLines(commit, changedFilePath, allEditedLineNumbers);
    }

    private void extractAPIsForModifiedLines(RevCommit commit, String changedFilePath, ArrayList<Integer> allEditedLineNumbers) throws IOException {
        /**
         * Extracts APIs associated with modified lines of a given commit
         */
        String commitCode = getCode(commit, changedFilePath);
        AbstractSyntaxTreeCrawler.getInstance().buildAST(commitCode, allEditedLineNumbers);
        if (commitAPIs.containsKey(commit.getName()))
            commitAPIs.get(commit.getName()).addAll(AbstractSyntaxTreeCrawler.getInstance().getAllAPIs());
        else
            commitAPIs.put(commit.getName(), AbstractSyntaxTreeCrawler.getInstance().getAllAPIs());
    }

    private void extractAPIsForDeletedLines(RevCommit commit, String changedFilePath, ArrayList<Integer> allDeletedLineNumbers) throws IOException {
        /**
         * Extracts APIs associated with deleted lines of a given commit
         */
        String parentCommitCode = getCode(commit.getParent(0), changedFilePath);
        AbstractSyntaxTreeCrawler.getInstance().buildAST(parentCommitCode, allDeletedLineNumbers);
        if (deletedCommitAPIs.containsKey(commit.getName()))
            deletedCommitAPIs.get(commit.getName()).addAll(AbstractSyntaxTreeCrawler.getInstance().getAllAPIs());
        else
            deletedCommitAPIs.put(commit.getName(), AbstractSyntaxTreeCrawler.getInstance().getAllAPIs());
    }

    private boolean extractAPIsForAddedFiles(RevCommit commit, DiffEntry diffEntry) throws IOException {
        /**
         * Newly added files must be considered separately, they are not presented in
         * modify changes in a commit.
         */
        ArrayList<Integer> lineNumbers = new ArrayList<>();
        // TODO: This is weird, but working for now!
        for (int i = 0; i < 100000; i++) {
            lineNumbers.add(i);
        }

        extractAPIsForModifiedLines(commit, diffEntry.getPath(DiffEntry.Side.NEW), lineNumbers);
        return false;
    }

    public Map<String, List<String>> getCommitAPIs() {
        return commitAPIs;
    }

    public Map<String, List<String>> getDeletedCommitAPIs() {
        return deletedCommitAPIs;
    }
}
