package buggycodeannotator;

import org.eclipse.jgit.diff.*;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.EmptyTreeIterator;
import org.eclipse.jgit.util.io.DisabledOutputStream;

import java.io.IOException;
import java.util.List;

/**
 * Created by smmsadrnezh on 12/11/16.
 */
public class DiffManager {

    private DiffFormatter diffFormatter;
    private Repository repository;

    public DiffManager(Repository repository) {
        /** Set diffFormatter */
        diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE);
        diffFormatter.setRepository(repository);
        this.repository = repository;
        diffFormatter.setDiffComparator(RawTextComparator.DEFAULT);
        diffFormatter.setDetectRenames(true);
    }

    public List<DiffEntry> getDiffEntries(RevCommit bugfixCommit) throws IOException {
        if (bugfixCommit.getParentCount() > 0) {
            return diffFormatter.scan(bugfixCommit.getParent(0).getTree(), bugfixCommit.getTree());
        } else {
            AbstractTreeIterator oldTreeIter = new EmptyTreeIterator();
            ObjectReader reader = repository.newObjectReader();
            AbstractTreeIterator newTreeIter = new CanonicalTreeParser(null, reader, bugfixCommit.getTree());
            return diffFormatter.scan(oldTreeIter, newTreeIter);
        }

    }

    public EditList getEdits(DiffEntry diffEntry) throws IOException {

        return diffFormatter.toFileHeader(diffEntry).toEditList();

    }
}
