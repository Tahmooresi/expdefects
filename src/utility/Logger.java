package utility;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hamed on 10/28/2017.
 */
public class Logger {
    public static void log(String message){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        System.out.println(timeStamp+" - "+message);
    }
}
