import pandas as pd
import os

BASE_RESULT_FOLDER = 'results/'
list_of_files = []
for f in os.listdir(BASE_RESULT_FOLDER):
    list_of_files.append(f)

list_of_files.sort(key=lambda x: int(x[x.index('_') + 1:x.index('.')]))

df = None
for path in list_of_files:
    tmp = pd.read_csv(BASE_RESULT_FOLDER + path)
    if df is None:
        df = tmp
    else:
        df = df.append(tmp)

df.to_csv('so_query_aggregated.csv')
